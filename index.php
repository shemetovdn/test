<!DOCTYPE html>
<html lang="en"  ng-app="imageLib">
<head>
    <meta charset="UTF-8">
    <title>библиотека изображений</title>
    <script src="/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="/assets/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/fancybox/jquery.fancybox.css" media="screen" />
<script src="/assets/js/angular.min.js"></script>
    <script src="/assets/js/angular-route.min.js"></script>
    <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
    <script src="/assets/js/app.js"></script>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/assets/fancybox/jquery.fancybox.css" media="screen" />
</head>
<body>
<div ng-controller="PaginationCtrl" ng-init="init()">
<form>
    <div class="form-group">
        <label for="exampleInputFile">File input</label>
        <input type="file" class="form-control-file" id="InputFile" aria-describedby="fileHelp">
    </div>
    <button type="submit" class="btn btn-primary" ng-click="uploadFile($event)">Upload</button>
</form>

    View <select ng-model="viewby" ng-change="setItemsPerPage(viewby)" class="form-control" style="width: 100px;">
        <option>3</option>
        <option>5</option>
        <option>10</option>
        <option>20</option>
        <option>30</option>
        <option>40</option>
        <option>50</option>
    </select> records at a time.

    <table class="table">
        <tr ng-repeat="row in image_list.slice(((currentPage-1)*itemsPerPage), ((currentPage)*itemsPerPage))  track by $index">
            <td>
                <a  href="/uploads/images/{{row.name}}" class="fancybox"><img src="/uploads/previews/{{row.name}}" ></a>
            </td>
            <td>{{row.width}}x{{row.height}}</td>
        </tr>
    </table>

    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" class="pagination-sm" items-per-page="itemsPerPage"></pagination>
</div>
</body>
</html>
