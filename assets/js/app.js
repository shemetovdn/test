var imageLib = angular.module('imageLib', ['ngRoute', 'ui.bootstrap']);

imageLib.directive('bindFile', [function () {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function ($scope, el, attrs, ngModel) {
            el.bind('change', function (event) {
                ngModel.$setViewValue(event.target.files[0]);
                $scope.$apply();
            });

            $scope.$watch(function () {
                return ngModel.$viewValue;
            }, function (value) {
                if (!value) {
                    el.val("");
                }
            });
        }
    };
}]);

imageLib.directive('fancybox', function($compile) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, element, attrs) {

            $scope.open_fancybox = function() {

                var el = angular.element(element.html()),

                    compiled = $compile(el);

                $.fancybox.open(el);

                compiled($scope);

            };
        }
    };
});

imageLib .controller('PaginationCtrl', function($scope,$http) {
    $scope.image_list = [];
    $scope.viewby = 10;
    $scope.totalItems = $scope.image_list.length;
    $scope.currentPage = 4;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 5;

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.setItemsPerPage = function(num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1;
    }

    $scope.init = function(){
        $http({
            method: 'POST',
            url: '/get-image.php',

            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {

            if (response.data != 0) {
                var images = [];
                $.each(response.data, function(i,n) {
                    images.push(n);});
                $scope.image_list = images;
                $scope.viewby = 10;
                $scope.totalItems = $scope.image_list.length;
            }

        });
        $('#InputFile').on('change', function(){
            $scope.files = this.files;
        });
    }

    $scope.uploadFile = function(event) {
        event.preventDefault();
        if(typeof $scope.files == 'undefined') return;
        if ($scope.files[0].size > 5000000) {
            alert("File  too big");return;
        }
        var data = new FormData();
        $.each( $scope.files, function( key, value ){
            data.append( key, value );
        });
        $.ajax({
            url         : './upload.php',
            type        : 'POST',
            data        : data,
            cache       : false,
            processData : false,
            contentType : false,
            success     : function(response){
                response = JSON.parse(response);
                if (response.errors) {
                    for(var i = 0; i <= response.errors.length; i++){
                        switch (response.errors[i]) {
                            case 1: alert("File not supported");
                            return;
                            case 2: alert("File  too big");
                                return;
                            case 3: alert("resolution is too large");
                                return;
                        }
                    }
                }
                if(response){
                    var images = [];
                    $.each(response, function(i,n) {
                        images.push(n);});
                    $scope.image_list = images;
                    $scope.$apply();
                    $scope.viewby = 10;
                    $scope.totalItems = $scope.image_list.length;
                }
                else {
                    console.log('ОШИБКА: ' + respond.error );
                }
            }
        });
    }
});
window.onload = function() {
    $("a.fancybox").fancybox();
}
