<?php
require_once(dirname(__FILE__).'/class.upload.php/src/class.upload.php');

class ImageHandler
{
    public $imageDir = './uploads/images';
    public $previewDir = './uploads/previews';
    public $tmpDir = './uploads/tmp';
    public $db ;

    function __construct()
    {
        if (!is_dir("./uploads/")) mkdir("./uploads/", 0777);
        if (!is_dir($this->tmpDir)) mkdir($this->tmpDir, 0777);
        $this->db = new PDO('mysql:host=localhost;dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
    }

    /**
     * @param $file
     * @return bool
     */
    public function uploadImage($file)
    {
            return move_uploaded_file($file['tmp_name'], "$this->tmpDir/".$file['name']);
    }

    /**
     * @param $handle
     * @param $fileName
     * @param bool $resize
     * @return bool
     */
    public function allocationImage($file)
    {
        $handle = new upload("$this->tmpDir/".$file['name']);
        if ($handle->uploaded) {
            $this->imageValidation($handle);
            $handle->file_overwrite = false;
            $handle->process($this->imageDir);
            if ($handle->processed) {
                $handle->image_resize = true;
                $handle->image_x = 100;
                $handle->image_y = 70;
                $handle->process($this->previewDir);
                $this->saveImageData($handle);
            }
        }

    }

    public function saveImageData($fileInfo)
    {
        $imageName = $fileInfo->file_dst_name;
        $imageSize = $fileInfo->file_src_size;
        $imageWidth = $fileInfo->image_src_x;
        $imageHeight = $fileInfo->image_src_y;
        try {
            $stmt = $this->db->prepare("INSERT INTO image (name) VALUES (?)");
            $stmt -> execute([$imageName]);
            $imageId = $this->db->lastInsertId();

            $this->saveImageAttribute($imageId, 'size', $imageSize);
            $this->saveImageAttribute($imageId, 'width', $imageWidth);
            $this->saveImageAttribute($imageId, 'height', $imageHeight);
            $this->saveImageAttribute($imageId, 'date', time());
        }
        catch(PDOException $e){
            echo 'Error : '.$e->getMessage();
            exit();
        }
    }

    /**
     * @param $imageId
     * @param $attributeName
     * @param $value
     */
    public function saveImageAttribute($imageId, $attributeName, $value)
    {
        $stmt = $this->db->prepare("INSERT INTO image_attribute (image_id, atribute_name, value) VALUES (?, ?, ?)");
        $stmt -> execute([$imageId, $attributeName, $value]);
    }

    public function getImageList()
    {
        $stmt = $this->db->query('SELECT image.id, image.name, image_attribute.atribute_name, image_attribute.value
 FROM image LEFT JOIN image_attribute ON image_attribute.image_id = image.id');
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        $images = array();
        foreach ($result as $key => $value) {
            $images[$value["id"]]["name"] = $value["name"];
            $images[$value["id"]]["id"] = $value["id"];
            $images[$value["id"]][$value["atribute_name"]] = $value["value"];
        }
        return json_encode($images);
    }
    public function imageValidation($handle)
    {
        $errors = [];
        if (!in_array($handle->file_src_name_ext, ['jpg', 'jpeg', 'bmp', 'png', 'gif'])) {
            $errors[] = 1;
        }
        if($handle->file_src_size > 5000000) {
            $errors[] = 2;
        }
        if ($handle->image_src_x >= 1920 && $handle->image_src_y >= 1080) {
            $errors[] = 3;
        }
        if (!empty($errors)) {
            if (file_exists("$this->tmpDir/" . $handle->file_src_name)) {
                unlink("$this->tmpDir/" . $handle->file_src_name);
            }
            echo json_encode(["errors" => $errors]);exit;
        }

    }
}