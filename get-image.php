<?php
require_once "config.php";
require_once(dirname(__FILE__).'/src/ImageHandler.php');
if (isset($_POST)) {
    $imageHandler = new ImageHandler();
    $imageList = $imageHandler->getImageList();
    echo $imageList;
}