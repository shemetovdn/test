<?php
require_once "config.php";
require_once(dirname(__FILE__).'/src/ImageHandler.php');
if ($_FILES) {
    $imageHandler = new ImageHandler();
    foreach($_FILES as $file){
        if ($imageHandler->uploadImage($file)) {
            if ($imageHandler->allocationImage($file)) {

            }
        }
    }
    echo $imageHandler->getImageList();
}